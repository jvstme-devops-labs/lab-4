FROM node:20.0-alpine3.17 as build

WORKDIR /app

COPY package*.json .
RUN npm ci

COPY . .
RUN npm run build --omit=dev


FROM nginx:1.24-alpine3.17
COPY --from=build /app/dist /usr/share/nginx/html
